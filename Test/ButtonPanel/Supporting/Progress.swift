//
//  Progress.swift
//  Test
//
//  Created by r00t on 13.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

/// **Progress** is a struct which is related with value from 0 -> 1. Can be changed. Using for transfare value of element's progress.
struct Progress {
    typealias ProgressValue = CGFloat
    
    var value: ProgressValue
    
    // Mirror reversing value
    var reversedValue: ProgressValue {
        return Progress.max - self.value
    }
    
    init(value: ProgressValue) {
        self.value = (value > Progress.max) ? Progress.max : ((value < Progress.min) ? Progress.min : value)
    }
    
    static var max: ProgressValue {
        return 1.0
    }
    
    static var min: ProgressValue {
        return 0.0
    }
    
    static var mid: ProgressValue {
        return self.max / 2
    }
    
    static var random: ProgressValue {
        return ProgressValue(arc4random_uniform(UInt32(Progress.max - Progress.min))) + Progress.min
    }
    
    
}
