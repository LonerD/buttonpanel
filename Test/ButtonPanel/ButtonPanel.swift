//
//  ButtonPanel.swift
//  Test
//
//  Created by r00t on 11.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

protocol ButtonPanelDelegate: class {
    var startPanningPosition: CGFloat { get set }
    func buttonPanelBeganPanning(module: ButtonPanelModuleBehaviour, gesture: UIPanGestureRecognizer)
    func buttonPanelChangedPanning(module: ButtonPanelModuleBehaviour, gesture: UIPanGestureRecognizer)
    func buttonPanelEndedPanning(module: ButtonPanelModuleBehaviour, gesture: UIPanGestureRecognizer)
    func buttonPanelCanceledPanning(module: ButtonPanelModuleBehaviour, gesture: UIPanGestureRecognizer)
    func didInteract(with Event: ComponentEventType, module: ButtonPanel.Modules, componentType: Components.ComponentsElements)
}

extension ButtonPanelDelegate {
    func buttonPanelCanceledPanning(module: ButtonPanelModuleBehaviour, gesture: UIPanGestureRecognizer) { }
}

class ButtonPanel: UIView, ButtonPanelAppearance {
    
   weak var delegate: ButtonPanelDelegate?
    
    // Components (UIView) of button panel
    var components: Components!
    
    // Current Module which is using
    var currentModule: ButtonPanelModuleBehaviour! {
        didSet {
            self.currentModule.setupOnDefault()
        }
    }
    
    /// INITS
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.prepare()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    /// Prepare button panel when init
    private func prepare() {
        self.setupComponents()
        self.addPanGesture()
        self.currentModule = Modules.timeline.object(self)
        self.setDefaultPosition()
        self.currentModule.setupOnDefault()
    }
}




// MARK: - Setup methods
extension ButtonPanel {
    
    fileprivate func setupComponents() {
        self.components = Components(parentView: self)
        self.observeCompronents()
    }
    
    fileprivate func addPanGesture() {
        self.components.setupPan(with: #selector(ButtonPanel.pan(gesture:)), target: self)
        self.gestureRecognizers?.forEach { self.removeGestureRecognizer($0) }
        self.addGestureRecognizer(self.components.panGesture.component)
    }
    
    fileprivate func observeCompronents() {
        let mainButton = self.components.mainButton
        let secondaryButton = self.components.secondaryButton
        
        mainButton.observe(onModule: Modules.chat.type) { event in
            self.delegate?.didInteract(with: event, module: .chat, componentType: .mainButton)
        }
        
        mainButton.observe(onModule: Modules.curtain.type) { event in
            self.delegate?.didInteract(with: event, module: .curtain, componentType: .mainButton)
        }
        
        mainButton.observe(onModule: Modules.timeline.type) { event in
            self.delegate?.didInteract(with: event, module: .timeline, componentType: .mainButton)
        }
        
        secondaryButton.observe(onModule: Modules.timeline.type) { event in
            self.delegate?.didInteract(with: event, module: .timeline, componentType: .secondaryButton)
        }
    }
    
    
}

// MARK: - ButtonPanel gesture handling
extension ButtonPanel {
    func pan(gesture: UIPanGestureRecognizer) {
        
        switch gesture.state {
        case .began:
            self.delegate?.buttonPanelBeganPanning(module: self.currentModule, gesture: gesture)
        case .changed:
            self.delegate?.buttonPanelChangedPanning(module: self.currentModule, gesture: gesture)
        case .ended:
            self.delegate?.buttonPanelEndedPanning(module: self.currentModule, gesture: gesture)
        case .cancelled:
            self.delegate?.buttonPanelCanceledPanning(module: self.currentModule, gesture: gesture)
        case .possible, .failed: return
        }
        
    }
}


// MARK: - Mudules of current button panel, can be expanded by adding a new.
// To extend, update 'moduleAssociatedType', 'returnType' methods
// ButtonPanelModule has convinienced init of appearance.
extension ButtonPanel {
     enum Modules {
        
        // All Types(Classes) of existing modules
        struct ModulesTypes {
            typealias timelineClassType = TimeLineButtonPanelModuleBehaviour
            typealias chatClassType = ChatButtonPanelModuleBehaviour
            typealias curtainClassType = CurtainButtonPanelModuleBehaviour
        }
        
        case timeline
        case chat
        case curtain
        
        func object(_ appearance: ButtonPanelAppearance) -> ButtonPanelModuleBehaviour {
            switch self {
            case .chat:
                return Modules.ModulesTypes.chatClassType(appearance)
            case .curtain:
                return Modules.ModulesTypes.curtainClassType(appearance)
            case .timeline:
                return Modules.ModulesTypes.timelineClassType(appearance)
            }
        }
        
        var type: NSObject.Type {
            switch self {
            case .chat:
                return Modules.chatType
            case .curtain:
                return Modules.curtainType
            case .timeline:
                return Modules.timelineType
            }
        }
        
        
        // Modules Types
        static var timelineType: ModulesTypes.timelineClassType.Type {
            return Modules.ModulesTypes.timelineClassType.self
        }
        
        static var chatType: ModulesTypes.chatClassType.Type {
            return Modules.ModulesTypes.chatClassType.self
        }
        
        static var curtainType: ModulesTypes.curtainClassType.Type {
            return Modules.ModulesTypes.curtainClassType.self
        }
        
        
    }
    
}


