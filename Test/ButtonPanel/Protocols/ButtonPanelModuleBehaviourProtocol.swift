//
//  ButtonPanelModuleProtocol.swift
//  Test
//
//  Created by r00t on 11.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

protocol ButtonPanelModuleBehaviour {
    var defaultCenterPosition: CGPoint { get }
    var appearance: ButtonPanelAppearance! { get set }
    var defaultButtonCenterXPosition: CGFloat { get }
    func setupOnDefault()
    
    func evaluate(with shiftProgress: Progress, from: ButtonPanelModuleBehaviour)
    init()
}

extension ButtonPanelModuleBehaviour {
    // TODO: throws Error
    func casted<S: NSObject>(_ Type: S.Type, value: ((S) -> ())) {
        guard let castedValue = self as? S else {
            return
        }
        value(castedValue)
    }
    
    init(_ appearance: ButtonPanelAppearance) {
        self.init()
        self.appearance = appearance
    }
}


extension ButtonPanelModuleBehaviour {
    
    var type: ButtonPanel.Modules {
        
        switch self {
        case is ButtonPanel.Modules.ModulesTypes.chatClassType: return .chat
        case is ButtonPanel.Modules.ModulesTypes.curtainClassType: return .curtain
        case is ButtonPanel.Modules.ModulesTypes.timelineClassType: return .timeline
        default: fatalError("Isnt available ModuleType")
        }
        
    }
    
}
