//
//  CurtainButtonPanelModule.swift
//  Test
//
//  Created by r00t on 11.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class CurtainButtonPanelModuleBehaviour: NSObject, ButtonPanelModuleBehaviour {
    
    required override init() { }
    
    
    var defaultCenterPosition: CGPoint {
        return CurtainConstants.shared.kDefaultCenterPosition
    }
    
    var defaultButtonCenterXPosition: CGFloat {
        return CurtainConstants.shared.kDefaultMainButtonCenterXPosition
    }
    
    var appearance: ButtonPanelAppearance!
    
    func evaluate(with shiftProgress: Progress, from: ButtonPanelModuleBehaviour) {
        let calculateShiftForValue: (_ from: CGFloat, _ to: CGFloat) -> CGFloat = { from, to in
            let shift = (to > from) ? max(from, to * shiftProgress.value) : min(from, max(to, from * shiftProgress.reversedValue))
            return shift
        }
        
        self.appearance.components.mainButton.component.center.x = calculateShiftForValue(from.defaultButtonCenterXPosition, self.defaultButtonCenterXPosition)
    }
    
    
    func setupOnDefault() {
        
    }
  
}


final class CurtainConstants {
    static private(set) var shared = CurtainConstants()
    
    var kDefaultCenterPosition: CGPoint {
        return CGPoint(x: UIScreen.main.bounds.width / 2, y: 100)
    }
    
    var kDefaultMainButtonCenterXPosition: CGFloat {
        return UIScreen.main.bounds.width / 2
    }
    
}
