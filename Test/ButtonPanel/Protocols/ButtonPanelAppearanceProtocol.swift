//
//  ButtonPanelAppearance.swift
//  Test
//
//  Created by r00t on 11.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

protocol ButtonPanelAppearance {
    var currentModule: ButtonPanelModuleBehaviour! { get set }
    var components: Components! { get set }
    mutating func changeModule(to: ButtonPanelModuleBehaviour, from: ButtonPanelModuleBehaviour, shiftProgress: Progress)
    func setDefaultPosition()
}

extension ButtonPanelAppearance where Self: UIView {
    mutating func changeModule(to: ButtonPanelModuleBehaviour, from: ButtonPanelModuleBehaviour, shiftProgress: Progress) {
        let toDefaultPosition = to.defaultCenterPosition.y
        let fromDefaultPosition = from.defaultCenterPosition.y
        let isToHigherThenTo = toDefaultPosition > fromDefaultPosition
        
        self.center.y = isToHigherThenTo ? max(fromDefaultPosition, toDefaultPosition * shiftProgress.value) : min(fromDefaultPosition, max(toDefaultPosition, fromDefaultPosition * shiftProgress.reversedValue))

        
        switch shiftProgress.value {
        case Progress.min:
            self.currentModule = from
        case Progress.max:
            self.currentModule = to
        default: break
        }
        
        to.evaluate(with: shiftProgress, from: from)
        from.evaluate(with: Progress(value: shiftProgress.reversedValue), from: to)
        
    }
    
    func setDefaultPosition() {
        UIView.animate(withDuration: 0.3) {
            self.center = self.currentModule.defaultCenterPosition
            self.components.mainButton.component.center.x = self.currentModule.defaultButtonCenterXPosition
        }
        
    }
    
}

