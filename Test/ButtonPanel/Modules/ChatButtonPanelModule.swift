//
//  ChatButtonPanelModule.swift
//  Test
//
//  Created by r00t on 11.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class ChatButtonPanelModuleBehaviour: NSObject, ButtonPanelModuleBehaviour {
    
    required override init() { }
    
    
    var defaultCenterPosition: CGPoint {
        return ChatConstants.shared.kDefaultCenterPosition
    }
    
    var defaultButtonCenterXPosition: CGFloat {
        return ChatConstants.shared.kDefaultMainButtonCenterXPosition
    }
    
    var appearance: ButtonPanelAppearance!
    
    func evaluate(with shiftProgress: Progress, from: ButtonPanelModuleBehaviour) {
        
        let calculateShiftForValue: (_ from: CGFloat, _ to: CGFloat) -> CGFloat = { from, to in
            let shift = (to > from) ? max(from, to * shiftProgress.value) : min(from, max(to, from * shiftProgress.reversedValue))
            return shift
        }

        self.appearance.components.mainButton.component.center.x = calculateShiftForValue(from.defaultButtonCenterXPosition, self.defaultButtonCenterXPosition)
    }
    
    func setupOnDefault() {
        self.appearance.components.mainButton.component.addTarget(self,
                                                                  action: #selector(ChatButtonPanelModuleBehaviour.didTapMainButton),
                                                                  for: .touchUpInside)
        
        UIView.animate(withDuration: 0.3) {
            self.appearance.components.mainButton.component.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.appearance.components.secondaryButton.component.alpha = 0
            self.appearance.components.timeLabel.component.alpha = 0
        }
    }
    
    func didTapMainButton() {
        self.appearance.components.mainButton.postNotification(of: self,
                                                               event: .touchInside)
    }
    
    
}


final class ChatConstants {
    static private(set) var shared = ChatConstants()
    
    var kDefaultCenterPosition: CGPoint {
        return CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 1.3)
    }
    
    var kDefaultMainButtonCenterXPosition: CGFloat {
        return UIScreen.main.bounds.width / 2
    }
    
}
