//
//  TimeLineButtonPanelModel.swift
//  Test
//
//  Created by r00t on 11.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class TimeLineButtonPanelModuleBehaviour: NSObject, ButtonPanelModuleBehaviour {
    
    required override init() { }

    var appearance: ButtonPanelAppearance!
    
    var defaultCenterPosition: CGPoint {
        if case .down = TimeLineConstants.shared.currentDirectionState {
            return TimeLineConstants.shared.kDirectionStateDownCenterPosition
        }
        else {
           return TimeLineConstants.shared.kDirectionStateUpCenterPosition
        }
    }
    
    var defaultButtonCenterXPosition: CGFloat {
        return TimeLineConstants.shared.kDefaultMainButtonCenterXPosition
    }
    
    func evaluate(with shiftProgress: Progress, from: ButtonPanelModuleBehaviour) {
        
        let calculateShiftForValue: (_ from: CGFloat, _ to: CGFloat) -> CGFloat = { from, to in
            let shift = (to > from) ? max(from, to * shiftProgress.value) : min(from, max(to, from * shiftProgress.reversedValue))
            return shift
        }
        
        self.appearance.components.mainButton.component.center.x = calculateShiftForValue(from.defaultButtonCenterXPosition, self.defaultButtonCenterXPosition)
    }
    
    
    func changePosition(_ position: CGPoint, animate Duration: Double = 0.1) {
        self.appearance.components.parentView.center.y = position.y
        UIView.animate(withDuration: Duration) {
            self.appearance.components.mainButton.component.center.x = position.x
        }
    }
    
    func setupOnDefault() {
        self.appearance.components.mainButton.component.addTarget(self,
                                                                  action: #selector(TimeLineButtonPanelModuleBehaviour.didTapMainButton),
                                                                  for: .touchUpInside)
        
        self.appearance.components.secondaryButton.component.addTarget(self,
                                                                       action: #selector(TimeLineButtonPanelModuleBehaviour.didTapSecondaryButton),
                                                                       for: .touchUpInside)
        
        UIView.animate(withDuration: 0.3) { 
            self.appearance.components.mainButton.component.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.appearance.components.secondaryButton.component.alpha = 1
            self.appearance.components.timeLabel.component.alpha = 1
        }
    }
    
    func didTapMainButton() {
        self.appearance.components.mainButton.postNotification(of: self,
                                                               event: .touchInside)
    }
    
    func didTapSecondaryButton() {
        self.appearance.components.secondaryButton.postNotification(of: self,
                                                                    event: .touchInside)
    }
    
    
    func changeTimeLabelText(to Time: String) {
        self.appearance.components.timeLabel.component.text = Time
    }
    
    func changeTimeLineScrollState(to State: TimeLineConstants.DirectionState, animate Duration: Double = 0.3) {
        TimeLineConstants.shared.currentDirectionState = State
        UIView.animate(withDuration: Duration) {
            switch State {
            case .down:
                self.appearance.components.parentView.center.y = TimeLineConstants.shared.kDirectionStateDownCenterPosition.y
            case .up:
                self.appearance.components.parentView.center.y = TimeLineConstants.shared.kDirectionStateUpCenterPosition.y
            }
        }
    }
    
}


final class TimeLineConstants {
    static private(set) var shared = TimeLineConstants()
    
    enum DirectionState {
        case up
        case down
    }
    
    var currentDirectionState: DirectionState = .up
    
    var kDirectionStateUpCenterPosition: CGPoint {
        return CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height * 0.3)
    }
    
    var kDefaultMainButtonCenterXPosition: CGFloat {
        return UIScreen.main.bounds.width * 0.8
    }
    
    var kDirectionStateDownCenterPosition: CGPoint {
        return CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height * 0.8)
    }
    
    
    
    
    
    
}
