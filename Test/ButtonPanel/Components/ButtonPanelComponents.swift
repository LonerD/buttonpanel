//
//  Components.swift
//  Test
//
//  Created by r00t on 12.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit
import SnapKit


protocol ButtonPanelComponents {
    var panGesture: Component<UIPanGestureRecognizer> { get set }
    var mainButton: Component<UIButton> { get set }
    var secondaryButton: Component<UIButton> { get set }
    var seperateLine: Component<UIView> { get set }
    var timeLabel: Component<UILabel> { get set }
}

enum ComponentEventType: String {

        case touchInside
        case longPress
        case didChangePosition
    
        case unknown
    
    static var key: String {
        return "event"
    }
}

struct Component<T> where T: NSObject {
    typealias ComponentType = T
    private let notifCenter = NotificationCenter.default
    
    var component: ComponentType
    
    init() {
        self.component = ComponentType.init()
    }

    func setup(_ onSetup: (ComponentType) -> ()) {
        onSetup(self.component)
    }
    
    func observe(onModule: NSObject.Type, _ onCallback: @escaping ((ComponentEventType) -> Void)) {
        self.notifCenter.addObserver(forName: Notification.Name(String(self.component.hashValue)),
                                               object: nil,
                                               queue: OperationQueue.current,
                                               using: { notification in
                                                notification.object.flatMap{$0 as? NSObject}
                                                    .map { ($0, onModule as AnyClass) }
                                                    .map { $0.0.isKind(of: $0.1) ?
                                                        (onCallback(ComponentEventType(rawValue: notification.userInfo?[ComponentEventType.key] as! String) ?? .unknown)) :
                                                        ()
                                                }
        })
    }
    
    func postNotification(of Object: ButtonPanelModuleBehaviour, event: ComponentEventType) {
        let notif = Notification(name: Notification.Name(String(self.component.hashValue)),
                                 object: Object,
                                 userInfo: [ComponentEventType.key : event.rawValue])
        self.notifCenter.post(notif)
    }
    
}


struct Components: ButtonPanelComponents {
    
    enum ComponentsElements {
        case mainButton
        case secondaryButton
        case seperateLine
        case timeLabel
        case buttonPanel
    }
    
    weak var parentView: UIView!
    
    var panGesture = Component<UIPanGestureRecognizer>()
    var mainButton = Component<UIButton>()
    var secondaryButton = Component<UIButton>()
    var seperateLine = Component<UIView>()
    var timeLabel = Component<UILabel>()
    
    init(parentView: UIView) {
        self.parentView = parentView
        self.prepare()
    }
    
    mutating func prepare() {
        self.initialSetup()
    }
    
    mutating func setupPan(with Selector: Selector, target: UIView) {
        self.panGesture.setup { pan in
            pan.addTarget(target, action: Selector)
        }
    }
    
    private mutating func initialSetup() {
        self.setupMainButton()
        self.setupSecondaryButton()
        self.setupSeperateLine()
        self.setupTimeLabel()
    }
    
    private func setupMainButton() {
        self.mainButton.setup { mainButton in
            mainButton.backgroundColor = UIColor.green
            mainButton.clipsToBounds = false
            self.parentView.addSubview(self.mainButton.component)
            
            let topAndBottomConstraint: CGFloat = 10
            let buttonSide = self.parentView.frame.height - topAndBottomConstraint * 2
            mainButton.frame = CGRect(x: 0,
                                      y: (self.parentView.frame.height / 2) - (buttonSide / 2),
                                      width: buttonSide,
                                      height: buttonSide)
            mainButton.layer.cornerRadius = mainButton.frame.width / 2
        }
        
    }
    
    private func setupSecondaryButton() {
        self.secondaryButton.setup { secondaryButton in
            secondaryButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            self.parentView.insertSubview(secondaryButton, belowSubview: self.mainButton.component)
            secondaryButton.snp.makeConstraints({ make in
                make.left.equalTo(20)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(40)
            })
            
            secondaryButton.clipsToBounds = false
            secondaryButton.layer.cornerRadius = 20
        }
    }
    
    private func setupSeperateLine() {
        self.seperateLine.setup { seperateLine in
            
            seperateLine.backgroundColor = UIColor.white
            self.parentView.addSubview(seperateLine)
            self.parentView.sendSubview(toBack: seperateLine)
            seperateLine.snp.makeConstraints({ make in
                make.left.right.centerY.equalToSuperview()
                make.height.equalTo(2)
            })
        }
    }
    
    private func setupTimeLabel() {
        self.timeLabel.setup { timeLabel in
            self.parentView.insertSubview(timeLabel, belowSubview: self.mainButton.component)
            timeLabel.text = "20:33"
            timeLabel.textColor = UIColor.white
            
            timeLabel.snp.makeConstraints({ make in
                make.left.equalTo(self.secondaryButton.component.snp.right).offset(15)
                make.bottom.equalTo(self.seperateLine.component.snp.top)
                make.height.equalTo(30)
                
            })
            
        }
    }
    
}
