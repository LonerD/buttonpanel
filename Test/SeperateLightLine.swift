//
//  SeperateLightView.swift
//  ContastsListLayuout
//
//  Created by r00t on 05.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class SeperateLightLine: UIView {
    
    private var lightGradient = CAGradientLayer()
    
    init(parentView Frame: CGRect, lineWidth: CGFloat = 2) {
        super.init(frame: CGRect(x: Frame.width - lineWidth,
                                 y: 0,
                                 width: lineWidth,
                                 height: Frame.height))
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setup() {
        self.backgroundColor = UIColor.clear
        self.setupGradientLight()
        
    }

    private func setupGradientLight() {
        self.updateLight(with: 0)
        
        self.lightGradient.colors = [UIColor(white: 1, alpha: 0).cgColor,
                                     UIColor(white: 1, alpha: 1).cgColor,
                                     UIColor(white: 1, alpha: 0).cgColor]
        self.lightGradient.frame = self.bounds
        self.lightGradient.locations = [0, 1, 1]
        self.lightGradient.startPoint = CGPoint(x: 0, y: 0.5)
        self.lightGradient.endPoint = CGPoint(x: 1, y: 0.5)
        
        self.layer.addSublayer(self.lightGradient)
    }
    
    // Stored property of light progress
    private var progressLightLeftSide: NSNumber = 0
    private var progressLightRightSide: NSNumber = 0
    
    
    /// Using progress reverse gradient colors (0->1:left->right)
    ///
    /// - Parameter Progress: progress scrolled
    func updateLight(with ProgressValue: CGFloat) {
        let reversingProgressCalcLeftSide = ((ProgressValue > Progress.mid) ? (Progress.mid - (Progress.mid - Progress(value: ProgressValue).reversedValue)) : ProgressValue)
        let reversingProgressCalcRightSide = ((ProgressValue < Progress.mid) ? (Progress.mid + (Progress.mid - Progress(value: ProgressValue).reversedValue)) : ProgressValue)
        self.progressLightRightSide = max(Progress.min, min(Progress.max, reversingProgressCalcRightSide)) as NSNumber
        self.progressLightLeftSide = max(Progress.min, min(Progress.max, reversingProgressCalcLeftSide)) as NSNumber
        let castedProgress = Progress(value: ProgressValue).reversedValue as NSNumber
        self.animateLocations(to: self.lightGradient, newLocations: [self.progressLightLeftSide,
                                                                     castedProgress,
                                                                     self.progressLightRightSide])
        
    }
    
   private func animateLocations(to GradientLayer: CAGradientLayer, newLocations: [NSNumber]) {
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = GradientLayer.locations
        animation.isRemovedOnCompletion = false
        animation.toValue = newLocations
        animation.duration = 0.01
        animation.fillMode = kCAFillModeForwards
        self.lightGradient.locations = newLocations
        GradientLayer.add(animation, forKey: "animationLocation")
    }
    
    
}
