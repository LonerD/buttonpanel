//
//  ViewController.swift
//  Test
//
//  Created by r00t on 10.07.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit


//BASEVIEWCONTROLLER
class ViewController: UIViewController {
    

    var buttonPanel: ButtonPanel!
    var startPanningPosition: CGFloat = 0
    
    
    
    var scroll: SVCScrollView!
    var separatorLine: SeperateLightLine!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepare()
    }

    
    func prepare() {
        self.setupButtonPanel()
        self.setupScrollViewControllers()
        self.setupSeparatorLine()
        self.observeScroll()
    }
    
    func setupScrollViewControllers() {
        let controllerOne = self.storyboard?.instantiateViewController(withIdentifier: "one") as? ViewControllerOne
        let controllerTwo = self.storyboard?.instantiateViewController(withIdentifier: "two") as? ViewControllerTwo
        self.scroll = SVCScrollView(frame: self.view.bounds, parentViewController: self, controllers: [controllerOne!, controllerTwo!])
        
        self.view.addSubview(self.scroll)
        self.view.sendSubview(toBack: self.scroll)
    }
    
    func setupSeparatorLine() {
        self.separatorLine = SeperateLightLine(parentView: self.view.frame,
                                              lineWidth: self.view.frame.width * 0.32)
        self.separatorLine.updateLight(with: 0)
        
        self.scroll.addSubview(self.separatorLine)
        
    }
    
    func setupButtonPanel() {
        self.buttonPanel = ButtonPanel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100))
        self.buttonPanel.delegate = self
        self.view.addSubview(buttonPanel)
    }
    
    func observeScroll() {
        self.scroll.didScrollPercent = { [weak self] progress in
            guard let panel = self?.buttonPanel else { return }
            
            self?.buttonPanel.changeModule(to: ButtonPanel.Modules.chat.object(panel),
                                          from: ButtonPanel.Modules.timeline.object(panel),
                                          shiftProgress: Progress(value: progress))
            
            self?.separatorLine.updateLight(with: progress)
            self?.separatorLine.frame.origin.x = ((self?.view.frame.maxX ?? 0) - (((self?.view.frame.maxX ?? 0) * Progress(value: progress).reversedValue) * 0.32))
        }
    }
    
}

extension ViewController: ButtonPanelDelegate {
    func buttonPanelBeganPanning(module: ButtonPanelModuleBehaviour, gesture: UIPanGestureRecognizer) {
        self.startPanningPosition = gesture.location(in: self.view).y - self.buttonPanel.center.y
        
        switch module.type {
            case .chat: return
            case .curtain: return
            case .timeline: return
        }
    }
    
    func buttonPanelChangedPanning(module: ButtonPanelModuleBehaviour, gesture: UIPanGestureRecognizer) {
        switch module.type {
            case .chat: return
            case .curtain: return
            case .timeline:
            
            self.buttonPanel.currentModule.casted(ButtonPanel.Modules.timelineType, value: { module in
                let position = CGPoint(x:  gesture.location(in: self.view).x, y: gesture.location(in: self.view).y - self.startPanningPosition)
                module.changePosition(position)
            })
            
        }
    }
    
    func buttonPanelEndedPanning(module: ButtonPanelModuleBehaviour, gesture: UIPanGestureRecognizer) {
        self.buttonPanel.setDefaultPosition()
    }
    
    func didInteract(with Event: ComponentEventType, module: ButtonPanel.Modules, componentType: Components.ComponentsElements) {
       print(Event, module, componentType)
    }
    
}

