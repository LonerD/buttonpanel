//
//  SVCScrollView.swift
//  Mediaplatforma
//
//  Created by Дмитрий Бондаренко on 11.10.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class SVCScrollView: UIScrollView, UIScrollViewDelegate {
    
    private var parentViewController: UIViewController!
    
    private var controllersToShowIntoScrollView: [UIViewController]!
    var visibleVC = 0
    
    var didScrollPercent: ((CGFloat)->())?
    
    init(frame: CGRect, parentViewController: UIViewController, controllers ToShow: [UIViewController]) {
        super.init(frame: frame)
        self.parentViewController = parentViewController
        self.controllersToShowIntoScrollView = ToShow
        self.addScrollView(viewControllers: ToShow)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var screenSize: CGSize {
        let screenBounds = UIScreen.main.bounds
        
        let width = screenBounds.width
        let height = screenBounds.height
        
        return CGSize(width: width, height: height)
    }
    
    
    private func addScrollView(viewControllers:[UIViewController]) {
        
        self.showsHorizontalScrollIndicator = false
        self.bounces = false
        
        self.contentSize = CGSize(width: self.screenSize.width * CGFloat(viewControllers.count),
                                  height: self.screenSize.height)
        self.isPagingEnabled = true
        self.delegate = self
        
        viewControllers.enumerated().forEach { index, viewController in
            self.parentViewController.addChildViewController(viewController)
            let originX = CGFloat(index) * self.screenSize.width
            viewController.view.frame = CGRect(x: originX,
                                               y: 0, width: self.screenSize.width,
                                               height: self.screenSize.height)
            self.addSubview(viewController.view)
            
            viewController.didMove(toParentViewController: self.parentViewController)
        }
    }
    
    
    func scrollToVC(number vc: Int, animate: Bool = true) {
        guard vc <= (controllersToShowIntoScrollView.count - 1) else {
            print("Sorry, but you cant slide view controller which isnt exist")
            return
        }
        visibleVC = vc
        self.setContentOffset(CGPoint(x: self.screenSize.width * CGFloat(vc), y: 0), animated: animate)
        
    }
    
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let persentScrolled = scrollView.contentOffset.x / screenSize.width
            self.visibleVC = Int(persentScrolled)
            self.didScrollPercent?(persentScrolled)
        }
   
    

}
